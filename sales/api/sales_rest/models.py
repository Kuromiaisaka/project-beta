from django.db import models
from django.urls import reverse

class Automobile (models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)

class SalesPerson (models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_sales_persons", kwargs={"pk": self.id})

class Customer (models.Model):
    name = models.CharField(max_length=100)
    address = models.TextField()
    phone_number = models.CharField(max_length=10)

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})

class SalesRecord (models.Model):
    automobile = models.ForeignKey(
        Automobile, 
        related_name="all_sales", 
        on_delete=models.CASCADE)

    sales_person = models.ForeignKey(
        SalesPerson, 
        related_name="all_sales", 
        on_delete=models.CASCADE)

    customer = models.ForeignKey(
        Customer, 
        related_name="all_sales", 
        on_delete=models.CASCADE)

    sale_price = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("api_sales_record", kwargs={"pk": self.id})
