from django.urls import path

from .api_views import (
    api_automobiles,
    api_customers,
    api_sales_record,
    api_delete_record,
    api_delete_person,
    api_sales_persons,
)

urlpatterns = [
    path("automobiles/", api_automobiles, name="api_automobiles"),
    path("customers/", api_customers, name="api_customers"),
    path("customers/<int:pk>/", api_customers, name="api_customer"),
    path("sales/", api_sales_record, name="api_sales_record"),
    path("sales_record/", api_delete_record, name="api_delete_record"),
    path("sales_persons/", api_sales_persons, name="api_sales_persons"),
    path("sales_persons/<int:pk>/", api_delete_person, name="api_delete_person")
]