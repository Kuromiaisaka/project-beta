from common.json import ModelEncoder
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json

from .models import Automobile, Customer, SalesPerson, SalesRecord

class AutomobileEncoder(ModelEncoder):
    model = Automobile
    properties = ["id", "vin", "import_href"]

@require_http_methods(["GET"])
def api_automobiles(request):
    if request.method == "GET":
        automobiles = Automobile.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileEncoder,
        )


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            people = Customer.objects.create(**content)
            return JsonResponse(
                people,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the customer"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            people = Customer.objects.get(id=pk)
            return JsonResponse(
                people,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            people = Customer.objects.get(id=pk)
            people.delete()
            return JsonResponse(
                people,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number",
    ]

class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "id",
        "automobile",
        "sales_person",
        "customer",
        "sale_price",
    ]
    encoders = {
        "automobile": AutomobileEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_sales_record(request):
    if request.method == "GET":
        sales = SalesRecord.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesRecordEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            print("hello",content)
            automobile = Automobile.objects.get(vin=content["automobile"])
            print(automobile)
            content["automobile"] = automobile

            sales_person = SalesPerson.objects.get(id=content["sales_person"])
            print(sales_person)
            content["sales_person"] = sales_person

            customer = Customer.objects.get(id=content["customer"])
            print(customer)
            content["customer"] = customer

        except:
            response = JsonResponse(
                {"message": "Could not create the sales record"}
            )
            response.status_code = 400
            return response

        sold = SalesRecord.objects.create(**content)
        return JsonResponse(
                sold,
                encoder=SalesRecordEncoder,
                safe=False,
            )

@require_http_methods(["DELETE", "GET"])
def api_delete_record(request, pk):
    if request.method == "GET":
        try:
            record = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            record = SalesRecord.objects.get(id=pk)
            record.delete()
            return JsonResponse(
                record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def api_sales_persons(request):
    if request.method == "GET":
        people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": people},
            encoder=SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            people = SalesPerson.objects.create(**content)
            return JsonResponse(
                people,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the sales person"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET"])
def api_delete_person(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            person = SalesPerson.objects.get(id=pk)
            person.delete()
            return JsonResponse(
                person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})