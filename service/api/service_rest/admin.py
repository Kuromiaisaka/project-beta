from django.contrib import admin

from .models import AutoVO, Technician, Appointment


admin.site.register(AutoVO)
admin.site.register(Technician)
admin.site.register(Appointment)
# Register your models here.
