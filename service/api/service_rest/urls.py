from django.urls import path

from .views import (
    api_get_appointment,
    api_list_appointments,
    api_get_technician,
    api_list_technicians,
    api_get_autovo,
    api_list_autovos,
)

urlpatterns = [
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path(
        "appointments/<int:pk>/",
        api_get_appointment,
        name="api_get_appointment",
    ),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:pk>/", api_get_technician, name="api_get_technician"),
    path("autos/", api_list_autovos, name="api_list_autovos"),
    path("autos/<int:pk>/", api_get_autovo, name="api_get_autovo"),
]