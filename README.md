# CarCar
    This application is designed for the use of employees at a car dealership, offering
    utility for sales, service. and inventory departments. 

# Team:
* Person 1 - Services (Taylor Panning)
* Person 2 - Sales (Stephen Ho)


# Running this application
  1. Run docker-compose build
  2. run docker-compose up
  3. test data will be needed to utilize all the features
       (add automobiles, models, manufacturers, technician, sales_reps, sales, and appointments)


     # things to note:
     -The (vin#) must be < 17 characters in length
     -When viewing the developer tools on the "sales person history" page, you are given
        an error that, "the keys must be unique." However, The keys are unique and this
        does not affect the use of this page.  


## Domain Driven Design
 "https://excalidraw.com/#room=8e0df1b39422b11f10ff,1IkFNiEVkRYMdkvbH2v1Mg"
above is a link to the excalidraw diagram of our planning using domain driven design.

in our design there is 4 seperate microservices.
    1. inventory
        1 value object used for both sales and service microservices (autoVO)-services // (automobile) in sales
    2. sales
    3. services
    4. react - front-end

    The root aggregate of the Service Microservice is the appointment* model
    The root aggregate of the Sales Microservice is the sale* model

    The use of microservices in this way allows the project to scale as needed by the 
    flow of traffic through our application.  



## Service microservice

My models will consist of a service_appointment and a salesVO. The sales VO will let me know if the car being serviced
was purchased from our dealership. If it was, A VIP tag will be added to the service appointment in our List of appointments. 
    The appointment model:
        vin# - foreign key (to salesValueObject)
        car_owner_name
        date_time (of appointment)
        reason_for_service
        assigned_technician (dropdown to select from created technicians)
        completed (default=No) changing to yes when button pressed
    
    The salesVO model:
        import_href:
        vin#


The front end of the Service Microservice will consist of a link to the "create a technician" form, a link to "create an appointment" form
and a link to the "list of all appointments". Inside the list of completed appointments will be a search bar to narrow the selection to a specific bin number. Within the list of all appointments table will be a button to "delete" appointment and "complete" appointment
If the appointment is marked complete it will be retained in the list of past services but no longer show up in the list of ongoing services

## Sales microservice

I will be making a sales, customer, and employee model. 
I need to make sure new cars can be created with the required
information. Employees will have their sales listed under their name. Recording a sale will update a car to be sold in the inventory and a new record to all sales/personal sales.