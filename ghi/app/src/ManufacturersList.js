import React from 'react';

class ManufacturersList extends React.Component {
    constructor(props){
        super(props);
        this.state={
            manufacturers: [],
        };
    }
    
    async componentDidMount() {
        const URL = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturers: data.manufacturers });
        }
    }
    render() {
        return (
    <div className="shadow p-4 mt-4">
        <h1>Available Makes</h1>
        <table className="table table-success table-striped">
            <thead>
                <tr>
                    <th>Manufacturers</th>
                </tr>
            </thead>
        <tbody>
           {this.state.manufacturers.map((manufacturer) => {
               return (
                   <tr key={manufacturer.href}>
                        <td>{ manufacturer.name }</td>
                    </tr>
            );
        })} 
        </tbody>
      </table>
    </div>
    );
  }
}

  
  export default ManufacturersList