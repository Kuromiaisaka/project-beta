
import React from "react";

class ServiceHistoryList extends React.Component {
    constructor(props){
        super(props);
        this.state={
            appointment: '',
            appointments: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleVinChange = this.handleVinChange.bind(this);
    }

    async componentDidMount() {
        const URL = 'http://localhost:8080/api/appointments/'
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ appointments: data.appointments.filter(each => each.completed === true) });
        }
    }

    async handleSubmit(event, vin) {
        event.preventDefault();
        console.log(event)
        console.log(vin)
        const filterURL = 'http://localhost:8080/api/appointments/'
        const response = await fetch(filterURL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ appointments: data.appointments.filter(each => each.completed === true) });
            this.setState({ appointments: data.appointments.filter(each => each.vin === vin) });
        }
    }
    handleVinChange(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }
    render() {
        return (
            <div className="shadow p-4 mt-4">
            <h1>Service History</h1>
            <div className="mb-3">
                <form className= "Vin" onSubmit={event => this.handleSubmit(event, this.state.vin)} id="filter-vin-lookup">
                    <input onChange={this.handleVinChange} required placeholder="Enter VIN" type="text" id="vin" name="vin" className="form-control" />
                    <label htmlFor="vin"></label>
                    <button className="btn btn-lg btn-primary">Search</button>
                </form>
            </div>
            <table className="table table-success table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Customer Name</th>
                        <th>VIP?</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
            <tbody>
                {this.state.appointments.map(appointment => {
                return (
                    <tr key={appointment.href}>
                        <td>{ appointment.vin }</td>
                        <td>{ appointment.car_owner_name }</td>
                        <td>{String(appointment.Vip)}</td>
                        <td>{ appointment.appointment_date }</td>
                        <td>{ appointment.appointment_time }</td>
                        <td>{ appointment.technician.name }</td>
                        <td>{ appointment.reason }</td>
                    </tr>
                );
                })}
            </tbody>
            </table>
        </div>



        );
 }
}

export default ServiceHistoryList;