import * as React from "react";
import ReactDOM from 'react-dom/client';
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
      <App />
      
    </React.StrictMode>
  );


async function loadInventory() {
  let manufacturersData, modelsData, automobilesData, techniciansData, appointmentsData
  const manufacturersResponse = await fetch('http://localhost:8100/api/manufacturers/');
  const modelsResponse = await fetch('http://localhost:8100/api/models/');
  const automobilesResponse = await fetch('http://localhost:8100/api/automobiles/');
  const techniciansResponse = await fetch('http://localhost:8080/api/technicians/');
  const AppointmentsResponse = await fetch('http://localhost:8080/api/appointments/');

  if (AppointmentsResponse.ok) {
    appointmentsData = await AppointmentsResponse.json();
    console.log('appointments data: ', appointmentsData)
  } else {
    console.error(AppointmentsResponse);  
  }
  if (techniciansResponse.ok) {
    techniciansData = await techniciansResponse.json();
    console.log('technicians data: ', techniciansData)
  } else {
    console.error(techniciansResponse);  
  }
  if (manufacturersResponse.ok) {
    manufacturersData = await manufacturersResponse.json();
    console.log('manufacturers data: ', manufacturersData)
  } else {
    console.error(manufacturersResponse);
  }
  if (modelsResponse.ok) {
    modelsData = await modelsResponse.json();
    console.log('model data: ', modelsData)
  } else {
    console.error(modelsResponse);
  }
  if (automobilesResponse.ok) {
    automobilesData = await automobilesResponse.json();
    console.log('automobile data: ', automobilesData)
  } else {
    console.error(automobilesResponse);
  }
}
loadInventory();
