import React from 'react';

class AppointmentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      
      car_owner_name: "",
      vin: "",
      appointment_time: "",
      appointment_date: "",
      reason: "",
      technicians: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeOwner = this.handleChangeOwner.bind(this);
    this.handleChangeVin = this.handleChangeVin.bind(this);
    this.handleChangeAppointmentTime = this.handleChangeAppointmentTime.bind(this);
    this.handleChangeAppointmentDate = this.handleChangeAppointmentDate.bind(this);
    this.handleChangeReason = this.handleChangeReason.bind(this);
    this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
  }


  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.technicians
    const TECHurl = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(TECHurl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      console.log(newAppointment);

      const cleared = {
        car_owner_name: "",
        vin: "",
        appointment_time: "",
        appointment_date: "",
        reason: "",
        technician: "",
      };
      this.setState(cleared);
    }
  }

  handleChangeOwner(event) {
    const value = event.target.value;
    this.setState({car_owner_name: value });
  }
  handleChangeVin(event) {
    const value = event.target.value;
    this.setState({vin: value });
  }
  handleChangeAppointmentTime(event) {
    const value = event.target.value;
    this.setState({appointment_time: value });
  }
  handleChangeAppointmentDate(event) {
    const value = event.target.value;
    this.setState({appointment_date: value });
  }
  handleChangeReason(event) {
    const value = event.target.value;
    this.setState({reason: value });
  }
  handleChangeTechnician(event) {
    const value = event.target.value;
    this.setState({technician: value });
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data.technicians });
    }
  }


  render() {
    return (
            <div className="shadow p-4 mt-4">
              <h1>Add A New Service Appointment</h1>
              <form onSubmit={this.handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                  <input onChange={this.handleChangeOwner} value={this.state.car_owner_name} placeholder="Car_owner_name" required type="text" name="car_owner_name" id="car_owner_name" className="form-control" />
                  <label htmlFor="car_owner_name">Car Owner's Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleChangeVin} value={this.state.vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                  <label htmlFor="vin">Car VIN</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleChangeAppointmentDate} value={this.state.appointment_date} placeholder="Appointment_date" required type="date" name="appointment_date" id="appointment_date" className="form-control" />
                  <label htmlFor="appointment_date">Appointment Date</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleChangeAppointmentTime} value={this.state.appointment_time} placeholder="Appointment_time" required type="time" name="appointment_time" id="appointment_time" className="form-control" />
                  <label htmlFor="appointment_time">Appointment Time</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={this.handleChangeReason} value={this.state.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                  <label htmlFor="reason">Reason For Visit</label>
                </div>
                <div className="mb-3">
                  <select onChange={this.handleChangeTechnician} value={this.state.technician} required name="technician" id="technician" className="form-select">
                    <option value="">Choose a Technician</option>
                    {this.state.technicians.map((technician) => {
                      return (
                        <option key={technician.href} value={technician.id}>
                            {technician.name} 
                        </option>
                      )
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Add</button>
              </form>
            </div>
      );
  }
}

export default AppointmentForm;