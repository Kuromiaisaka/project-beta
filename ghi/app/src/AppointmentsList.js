import React from 'react';

class AppointmentsList extends React.Component {
    constructor(props){
        super(props);
        this.state={
            appointments: [],

        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
    }

    async componentDidMount() {
        const URL = 'http://localhost:8080/api/appointments/'
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ appointments: data.appointments.filter(each => each.completed === false) });
        }
    }
    async handleDelete(event,appointment) {
        event.preventDefault();

        const appointmentIdArray = appointment.split("/");
        const appID = appointmentIdArray[3];
        const AppointmentUrl = `http://localhost:8080/api/appointments/${appID}/`;
        const fetchConfig = {
          method: "delete",
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(AppointmentUrl, fetchConfig);
        if (response.ok) {
            console.log("delete success")
    }
}
    async handleUpdate(event,appointment) {
        event.preventDefault();

        const appointmentIdArray = appointment.href.split("/");
        const appID = appointmentIdArray[3];
        const AppointmentUrl = `http://localhost:8080/api/appointments/${appID}/`;
        const data = { 
            "href": appointment.href,
            "car_owner_name": appointment.car_owner_name,
            "appointment_date": appointment.appointment_date,
            "appointment_time": appointment.appointment_time,
            "reason": appointment.reason,
            "vin": appointment.vin,
            "technician": appointment.technician.name,
            "completed": true,
            "Vip": String(appointment.Vip),}
        const fetchConfig = {
        method: "put",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    };
        const response = await fetch(AppointmentUrl, fetchConfig);
        if (response.ok) {
            console.log("appointment completed")
    }
}

    render() {
        return (
            <div className="shadow p-4 mt-4">
            <h1>Service Appointments</h1>
            <table className="table table-success table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Customer Name</th>
                        <th>VIP?</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th> </th>
                        <th> </th>
                    </tr>
                </thead>
            <tbody>
                {this.state.appointments.map(appointment => {
                return (
                    <tr key={appointment.href}>
                        <td>{ appointment.vin }</td>
                        <td>{ appointment.car_owner_name }</td>
                        <td>{String(appointment.Vip)}</td>
                        <td>{ appointment.appointment_date }</td>
                        <td>{ appointment.appointment_time }</td>
                        <td>{ appointment.technician.name }</td>
                        <td>{ appointment.reason }</td>
                        <td>
                            <button onClick={event => this.handleUpdate(event,appointment)} type="button" className="btn btn-success">Mark Done</button>
                        </td>
                        <td>
                            <button onClick={event => this.handleDelete(event,appointment.href)} type="button" className="btn btn-danger">Delete</button>
                        </td>
                    </tr>
                );
                })}
            </tbody>
            </table>
        </div>



        );
 }
}

export default AppointmentsList;