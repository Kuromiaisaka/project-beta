import React from 'react';

class AutomobilesList extends React.Component {
    constructor(props){
        super(props);
        this.state={
            automobiles: [],
        };
    }
    
    async componentDidMount() {
        const URL = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ automobiles: data.automobiles });
        }
    }
    render() {
        return (
    <div className="shadow p-4 mt-4">
        <h1>Available Automobiles</h1>
        <table className="table table-success table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
        <tbody>
           {this.state.automobiles.map((auto) => {
               return (
                   <tr key={ auto.href }>
                        <td>{ auto.vin }</td>
                        <td>{ auto.color }</td>
                        <td>{ auto.year }</td>
                        <td>{ auto.model.name }</td>
                        <td>{ auto.model.manufacturer.name }</td>
                    </tr>
            );
        })} 
        </tbody>
      </table>
    </div>
    );
  }
}

  
export default AutomobilesList