import { NavLink } from 'react-router-dom';
import { Dropdown, Navbar } from 'react-bootstrap';
import React from 'react'
import './index.css';

function Nav() {
  return (
    <Navbar className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <Dropdown as={Nav.Item} >
            <Dropdown.Toggle className="navbar-dark bg-success my-dropdown">Inventory</Dropdown.Toggle>
            <Dropdown.Menu className="navbar-dark bg-success">
              <Dropdown.Item>
                <NavLink className="nav-link" to="/automobiles">Automobile list</NavLink>
              </Dropdown.Item>
              <Dropdown.Item>
                <NavLink className="nav-link" to="/automobiles/new">Add Automobile</NavLink>
              </Dropdown.Item>
              <Dropdown.Item>
                <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
                </Dropdown.Item>
              <Dropdown.Item >
                <NavLink className="nav-link" to="/manufacturers/new">Add Manufacturer</NavLink>
                </Dropdown.Item>
              <Dropdown.Item>
              <NavLink className="nav-link" to="/models">Car Models</NavLink>
              </Dropdown.Item>
              <Dropdown.Item>
              <NavLink className="nav-link" to="/models/new">Add Car Models</NavLink>
              </Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>
      <Dropdown as={Nav.Item}>
        <Dropdown.Toggle className="navbar-dark bg-success my-dropdown">Service</Dropdown.Toggle>
        <Dropdown.Menu className="navbar-dark bg-success">
            <Dropdown.Item>
              <NavLink className="nav-link" to="/technicians">Technicians</NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="nav-link" to="/servicehistory">Service History</NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="nav-link" to="/technicians">Enter A Technician</NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="nav-link" to="/appointments">Appointments List</NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="nav-link" to="/appointments/new">Enter A Appointment</NavLink>
            </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
      <Dropdown as={Nav.Item}>
        <Dropdown.Toggle className="navbar-dark bg-success my-dropdown">Sales</Dropdown.Toggle>
        <Dropdown.Menu className="navbar-dark bg-success">
            <Dropdown.Item>
              <NavLink className="nav-link" to="/customer">New Customer</NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="nav-link" to="/saleshistory">Employee sales</NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="nav-link" to="/salesperson">New Sales Person</NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="nav-link" to="/allsales">All Sales</NavLink>
            </Dropdown.Item>
            <Dropdown.Item>
              <NavLink className="nav-link" to="/newsale">New Sale</NavLink>
            </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
          </ul>
        </div>
      </div>
    </Navbar>
  )
}

export default Nav;
