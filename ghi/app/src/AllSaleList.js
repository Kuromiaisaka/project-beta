import React from 'react';

class AllSaleList extends React.Component {
    constructor(props){
        super(props);
        this.state={
            sales: [],
        };
    }

    async componentDidMount() {
        const URL = 'http://localhost:8090/api/sales/'
        const response = await fetch(URL);
        if (response.ok) {
            const data = await response.json();
            this.setState({ sales: data.sales });
        }
    }

    render() {
        return (
            <div className="shadow p-4 mt-4">
                <h1>All Branch Sales</h1>
            <table className="table table-success table-striped">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Employee Number</th>
                        <th>Purchaser's Name</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
            <tbody>
                {this.state.sales.map(sale => {
                    return (
                        <tr key={sale.href}>
                    <td>{ sale.sales_person.name}</td>
                    <td>{ sale.sales_person.employee_number }</td>
                    <td>{ sale.customer.name }</td>
                    <td>{ sale.automobile.vin }</td>
                    <td>${ sale.sale_price }</td>
                    </tr>
                );
            })}
            </tbody>
            </table>
            </div>
        );
 }
}

export default AllSaleList;