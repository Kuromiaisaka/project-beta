function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
      </div>
      <img src='https://images.pexels.com/photos/164634/pexels-photo-164634.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1://www.shutterstock.com/shutterstock/photos/1791360410/display_1500/stock-photo-young-african-american-woman-professional-female-mechanic-examining-under-hood-of-car-with-torch-1791360410.jpg' alt="" style={{display: 'block', margin: 'auto'}} ></img>
    </div>
  );
}

export default MainPage;
