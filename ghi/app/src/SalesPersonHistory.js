import React from 'react';
class SalesPersonHistory extends React.Component {
    constructor(props){
        super(props);
        this.state={
            sales:[],
        };
        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
    }

    async componentDidMount() {
      const URL2 = 'http://localhost:8090/api/sales/'
      const response = await fetch(URL2);
      if (response.ok) {
        const data = await response.json();
        this.setState({ sales: data.sales });
      }
    }

    async handleUpdate(event){
      event.preventDefault();
      const value = event.target.value;
      console.log("pulled target value", value)

      const filterURL = 'http://localhost:8090/api/sales/'
        const response = await fetch(filterURL);
        if (response.ok) {
          const data = await response.json();
          console.log("data.sales", data.sales)
            this.setState({ sales: data.sales.filter(sale => sale.sales_person.name === value) });
            console.log("sales", this.state.sales)
        }
    }

    handleSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({ sales_person: value });
      }

    render() {
        return (
            <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Salesperson History</h1>
            <div className="mb-3">
                  <select onChange={this.handleUpdate} value={this.state.sales.sales_person} required name="sales_person" id="sales_person" className="form-select">
                    <option value="">Choose a Sales Person</option>
                    {this.state.sales.map((sale) => {
                      return (
                        <option key={sale.sales_person.id} value={sale.sales_person.href}>
                            {sale.sales_person.name} 
                        </option>
                      )
                    })}
                  </select>
                </div>
        <table className="table table-success table-striped">
            <thead>
                <tr>
                    <th>Sales Person</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Sales Price</th>
                </tr>
            </thead>
        <tbody>
        {this.state.sales.map(sale => {
                return (
                    <tr key={sale.href}>
                    <td>{ sale.sales_person.name}</td>
                    <td>{ sale.customer.name }</td>
                    <td>{ sale.automobile.vin }</td>
                    <td>${ sale.sale_price }</td>
                    </tr>
                );
                })}
        </tbody>
      </table>
      </div>
      </div>
      </div>
    );
  }
}
  export default SalesPersonHistory