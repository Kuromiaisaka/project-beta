import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import ManufacturersForm from './ManufacturersForm';
import AutomobilesList from './AutomobilesList';
import AutomobilesForm from './AutomobilesForm';
import ModelsList from './ModelsList';
import CustomerForm from './CustomerForm';
import SalesPersonForm from './SalesPersonForm';
import NewSaleForm from './NewSaleForm';
import ModelsForm from './ModelsForm';
import SalesPersonHistory from './SalesPersonHistory';
import TechnicianForm from './NewTechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentsList from './AppointmentsList';
import AllSaleList from './AllSaleList';
import ServiceHistoryList from './ServiceHistoryList';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturersList manufacturers={props.manufacturers} />}/>
            <Route path="new" element={<ManufacturersForm />}/>
          </Route>
          <Route path="technicians">
            <Route index element={<TechnicianForm technicians={props.technicians} />}/>
            <Route path="new" element={<TechnicianForm />}/>
          </Route>
          <Route path="customer">
            <Route index element={<CustomerForm customers={props.customers}/>}/>
          </Route>
          <Route path="salesperson">
            <Route index element={<SalesPersonForm sales={props.sales}/>}/>
          </Route>
          <Route path="newsale">
            <Route index element={<NewSaleForm newSales={props.newSales}/>}/>
          </Route>
            <Route path="models">
             <Route index element={<ModelsList models={props.models} />}/>
             <Route path="new" element={<ModelsForm />}/>
            </Route>
          <Route path="saleshistory">
            <Route index element={<SalesPersonHistory saleshistory={props.saleshistory}/>}/>
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesList automobiles={props.automobiles} />}/>
            <Route path="new" element={<AutomobilesForm />}/>
          </Route> 
          <Route path="appointments">
            <Route index element={<AppointmentsList appointments={props.appointments} />}/>
            <Route path="new" element={<AppointmentForm />}/>
          </Route>
          <Route path="allsales">
            <Route index element={<AllSaleList allSales={props.allSales}/>}/>
          </Route>
          <Route path="servicehistory">
            <Route index element={<ServiceHistoryList servicehistory={props.servicehistory}/>}/>
          </Route>
        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;
